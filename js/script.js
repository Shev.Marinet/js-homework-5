let key = null;
let box = null;

const user= {
    firstName: `Andrey`,
    lastName: `Shevchenko`,
    age: 42,
    contacts: {
        phone : `+38(097)111-11-11`,
        email : `aShevchenko@gmail.com`
    },
    getFullName: function() {
        return `${this.firstName} ${this.lastName}`}
};

function cloneObject(obj) {
    let clone = Array.isArray(obj) ? [] : {};
    for(key in obj) {
        box = obj[key];
        clone[key] = (typeof box === "object" && box !== null) ? cloneObject(box) : box;
    }
    return clone;
}

const user2 = cloneObject(user);
user2.age =29;
user2.lastName = `Yarmolenko`;
user2.contacts.phone = `+38(097)222-22-22`;
user2.contacts.email = `aYarmolenko@gmail.com`;
console.log(user);
console.log(user2);
console.log(user.getFullName());
console.log(user2.getFullName());

